cd $CI_PROJECT_DIR
set -o errexit -o nounset -o noglob
# Not in dash: -o pipefail
(. /etc/os-release ; echo $PRETTY_NAME )
set -o xtrace

export USER=nixbld


if ! id -g nixbld &> /dev/null ; then
    ${SUDO:-} apt-get update --quiet=2
    ${SUDO:-} apt-get install \
            --no-install-suggests \
            --no-install-recommends \
            --quiet=2 \
        apt-utils \
        moreutils \
        > /dev/null
    CHRONIC=chronic
    ${CHRONIC:-} ${SUDO:-} apt-get install \
            --no-install-suggests \
            --no-install-recommends \
            --quiet=2 \
        ca-certificates \
        catatonit \
        nix-bin \
        ocaml \
        ocamlbuild \
        ocaml-findlib \
        opam \
        sudo \
    ;
    chmod a+x scripts/group_user/group_useradd.sh
    scripts/group_user/group_useradd.sh \
        $USER
    echo "nixbld ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER
    fi

nix-channel --version
nix-shell --version
nix-channel \
    --add https://nixos.org/channels/$NIXOS_CHANNEL \
        nixpkgs
nix-channel --update

command -v catatonit
command -v setpriv

exec catatonit -- \
nix-shell \
        --packages fstar \
        --run "sh" \
        --quiet

exec catatonit -- \
    setpriv \
        --reuid=$(id -u nixbld) \
        --regid=$(id -g nixbld) \
        --init-groups \
        -- \
    nix-shell \
        --packages fstar \
        --run "sh" \
        # --quiet

#! /bin/sh
set -o errexit -o nounset -o noglob
# Not in dash: -o pipefail
set -o xtrace


groupadd --system nixbld

echo $@

useradd \
    --comment "Nix build user" \
    --create-home \
    --gid nixbld \
    --groups nixbld \
    --no-user-group \
    --system \
    --shell "$(which nologin)" \
    $1

install \
            --owner=$1 \
            --group=nixbld \
            --directory \
        /nix \
        /nix/var \
        /nix/var/nix

